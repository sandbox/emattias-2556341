(function($){

  var $form,
      $InstantPreview_outputWrapper,
      $InstantPreview_outputLoaderWrapper,
      $InstantPreview_outputLoader,
      $InstantPreview_output,
      $InstantPreview_formIframe,
      $InstantPreview_button,
      $InstantPreview_wrapperAll,
      $InstantPreview_handle,
      $InstantPreview_iframeFixAll,
      $InstantPreview_iframeFix__form,
      $InstantPreview_iframeFix__output,
      $InstantPreview_messagesWrapper,
      $InstantPreview_messagesElement = [],
      $InstantPreview_contentWrapper,
      $InstantPreviewParentDocument,
      fullWidth,
      isPreviewing = false,
      fullPageRenderIsDone = false,
      messagesElementSelector,
      messagesWrapperInDom = false,
      listenforWindowResize = true,
      formXhr,
      keyUpDebounceWaitTime = 300;

  InstantPreview = {
    resizeElements: function(event, ui){
      var offset;

      if(ui){
        offset = ui.offset;
      }
      else{
        offset = $InstantPreview_handle.offset();
      }

      var formWidth = (offset.left / fullWidth) * 100 + '%',
          outputWidth = ((fullWidth - (offset.left + 10)) / fullWidth) * 100 + '%';

      $InstantPreview_formIframe.width(formWidth);
      $InstantPreview_iframeFix__form.width(formWidth);

      $InstantPreview_outputWrapper.width(outputWidth);
      $InstantPreview_iframeFix__output.width(outputWidth);
    },
    enterPreviewingMode: function(){

      if($InstantPreview_outputWrapper.width() == 0){
        InstantPreview.removeJqueryUIDragStyleAttributes();
      }

      $InstantPreview_button.val(Drupal.t('Stop Previewing'));
      $InstantPreview_wrapperAll.addClass('is-previewing');
      isPreviewing = true;
    },
    updatePreview: function(){

      if(isPreviewing){
        $InstantPreview_outputLoaderWrapper.addClass('is-loading');
      }

      var data = {};
      if(!fullPageRenderIsDone){
        data['fullPageRenderIsDone'] = fullPageRenderIsDone;
        fullPageRenderIsDone = true;
      }

      if(formXhr && formXhr.readyState != 4){
        formXhr.abort();
      }

      $form.ajaxSubmit({
        url: Drupal.settings.basePath + 'instant_preview/ajax',
        dataType: 'json',
        type: 'POST',
        data: data,
        success: function(response, status){

          if(response.isFullPageRender){
            $InstantPreview_output.attr('srcdoc', response.data);
          }
          else{
            $InstantPreview_contentWrapper = $InstantPreview_contentWrapper || $InstantPreview_output.contents().find('.js-InstantPreview-contentWrapper');

            $InstantPreview_contentWrapper
              .html(response.data);
          }

          $InstantPreview_outputLoaderWrapper.fadeOut(200, function(){
            $(this).removeClass('is-loading');
          });


          if(!messagesWrapperInDom){
            messagesElementSelector = messagesElementSelector || response.messagesElementSelector;
            $InstantPreview_messagesElement = $InstantPreview_output.contents().find(messagesElementSelector);
            $InstantPreview_messagesWrapper = $InstantPreview_messagesWrapper || $('<div class="js-InstantPreview-messagesWrapper" />');

            if($InstantPreview_messagesElement.length > 0){
              $InstantPreview_messagesElement.replaceWith($InstantPreview_messagesWrapper);
              messagesWrapperInDom = true;
            }
          }

          if(('messages' in response) && $InstantPreview_messagesWrapper.length > 0){
            $InstantPreview_messagesWrapper.html(response.messages);
          }

        }
      });

      formXhr = $form.data('jqxhr');
    },
    removeJqueryUIDragStyleAttributes: function(){
      $InstantPreview_formIframe.removeAttr('style');
      $InstantPreview_outputWrapper.removeAttr('style');
      $InstantPreview_handle.removeAttr('style');
      $InstantPreview_wrapperAll.removeClass('is-previewing is-transitionsDisabled');
    },
    closePreview: function(){
      isPreviewing = false;
      $InstantPreview_button.val(Drupal.t('Preview'));

      InstantPreview.removeJqueryUIDragStyleAttributes();
    },
    excludeProcessed: function(i, elem){
      var $this = $(this);

      if($this.data('InstantPreview-processed')){
        return false;
      }

      $this.data('InstantPreview-processed', true);
      return true;
    },
    setFullWidth: function(){
      $InstantPreviewParentDocument = $InstantPreviewParentDocument || $(window.parent.document);
      fullWidth = $InstantPreviewParentDocument.width();
    }
  };

  $(window).on('resize.InstantPreview', _.debounce(function(){
    if(!listenforWindowResize){
      return;
    }
    InstantPreview.setFullWidth();
  }, 150));

  $(window).load(function(){
    InstantPreview.updatePreview();
  });

  InstantPreview.setFullWidth();

  $(function(){

    $(window).on('click', 'a', function(e,b,x) {
      // @TODO: Do this another way?
      $(e.currentTarget).attr('target', '_top');
    });

    CKEDITOR.on('instanceReady', function(event)
    {
      event.editor.on( 'change', _.debounce(function() {
        $(this.element.$).val(this.getData());
        InstantPreview.updatePreview();
      }, keyUpDebounceWaitTime));
    });

  });

  var fids = {};
  Drupal.behaviors.instantPreview = {
    attach: function (context, settings) {
      $form = $form || $('.js-InstantPreview-form', context);
      $InstantPreview_button = $InstantPreview_button || $form.find('.js-InstantPreview-button');
      $InstantPreview_output = $InstantPreview_output || $('.js-InstantPreview-output', window.parent.document);
      $InstantPreview_outputWrapper = $InstantPreview_outputWrapper || $('.js-InstantPreview-outputWrapper', window.parent.document);
      $InstantPreview_outputLoaderWrapper = $InstantPreview_outputLoaderWrapper || $('.js-InstantPreview-outputLoaderWrapper', window.parent.document);
      $InstantPreview_outputLoader = $InstantPreview_outputLoader || $('.js-InstantPreview-outputLoader', window.parent.document);
      $InstantPreview_handle = $InstantPreview_handle || window.parent.$('.js-InstantPreview-handle', window.parent.document);
      $InstantPreview_wrapperAll = $InstantPreview_wrapperAll || $('.js-InstantPreview-wrapperAll', window.parent.document);
      $InstantPreview_iframeFix__form = $InstantPreview_iframeFix__form || $('.js-InstantPreview-iframeFix--form', window.parent.document);
      $InstantPreview_iframeFix__output = $InstantPreview_iframeFix__output || $('.js-InstantPreview-iframeFix--output', window.parent.document);
      $InstantPreview_iframeFixAll = $InstantPreview_iframeFixAll || $InstantPreview_iframeFix__form.add($InstantPreview_iframeFix__output);
      $InstantPreview_formIframe = $InstantPreview_formIframe || $('.js-InstantPreview-formIframe', window.parent.document);

      $InstantPreview_handle
        .filter(InstantPreview.excludeProcessed)
        .draggable({
          addClasses: false,
          axis: 'X',
          containment: $InstantPreview_wrapperAll,
          drag: function(event, ui){
            InstantPreview.resizeElements(event, ui);

            if(!isPreviewing && $InstantPreview_outputWrapper.width() > 0){
              InstantPreview.enterPreviewingMode();
            }
          },
          start: function( event, ui ) {
            listenforWindowResize = false;
            $InstantPreview_wrapperAll.addClass('is-transitionsDisabled');
            $InstantPreview_iframeFixAll.show();
          },
          stop: function( event, ui ) {
            listenforWindowResize = true;
            $InstantPreview_wrapperAll.removeClass('is-transitionsDisabled');
            $InstantPreview_iframeFixAll.hide();

            if($InstantPreview_outputWrapper.width() == 0){
              InstantPreview.closePreview();
            }

            var left = $InstantPreview_handle.css('left');
            $InstantPreview_handle.css('left', parseInt(left) / (fullWidth / 100) + '%');
          }
        })
        .click(function(){
          $InstantPreview_button.click();
        });

      $form
        .find('input[type=text]:not(.js-InstantPreview-autocomplete), input[type=password], textarea')
          .filter(InstantPreview.excludeProcessed)
          .keyup(_.debounce(function (e) {
            if(e.keyCode == 9 || e.keyCode == 16){
              return;
            }

            InstantPreview.updatePreview();
          }, keyUpDebounceWaitTime));

      $InstantPreview_button
        .filter(InstantPreview.excludeProcessed)
        .on('click', function(e){
          e.preventDefault();

          if(isPreviewing){
            InstantPreview.closePreview();
          }
          else{
            InstantPreview.removeJqueryUIDragStyleAttributes();
            InstantPreview.enterPreviewingMode();
          }

        });

      $form
        .find('.js-InstantPreview-autocomplete')
        .filter(InstantPreview.excludeProcessed)
        .on('autocompleteSelect', function(){
          InstantPreview.updatePreview();
        });

      $form
        .find('input[type=radio], input[type=checkbox], select')
          .filter(InstantPreview.excludeProcessed)
          .change(function(){
          InstantPreview.updatePreview();
          });

      $form
        .find('.js-InstantPreview-fidElement')
        .each(function (i, e) {
          var $this = $(this),
            name = $this.attr('name'),
            val = $this.val();

          if ((name in fids && fids[name] != val) || (!(name in fids ) && val != 0)) {
            InstantPreview.updatePreview();
          }

          fids[name] = val;
        });
    }
  };

})(jQuery);